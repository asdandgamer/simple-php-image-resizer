<?php /*
*   file:   paste_image.php(AJAX)
*   autor:  asdandgamer@gmail.com
*   create:	15.06.2018
*   description: save pasted from clipboard image on server and resize it, [quick tool]
*/
ini_set("display_errors", true);

class IGetProps
{
    public static function GetProps($sWidth, $sHeight, $tSize)
    {
        $getProps = new GetProps($sWidth, $sHeight, $tSize);
        return $getProps->Props();
    }
}

class GetProps
{
    private $sWidth;		// sorce image width
	private $sHeight;		// sorce image height
	private $tWidth;
    private $tHeight;

    function __construct($sWidth, $sHeight, $tSize)
    {
        $this->sWidth = $sWidth;
        $this->sHeight = $sHeight;
        $this->GetProportionsSize($tSize);
    }

    private function GetProportionalWidth($tSize)
	{
		$x = ($tSize * $this->sWidth) / $this->sHeight;
		return round($x);
	}
	private function GetProportionalHeight($tSize)
	{
		$x = ($tSize * $this->sHeight) / $this->sWidth;
		return round($x);
	}
	private function GetProportionsSize($tSize)
	{
        if($this->sWidth < $tSize && $this->sHeight < $tSize) {
            $this->tWidth = $this->sWidth;
            $this->tHeight = $this->sHeight;
            return;
        }
        //-----------------------------------------------------------
        if($this->sWidth == $this->sHeight) {
            $this->tWidth = $tSize;
			$this->tHeight = $tSize;
        } else if($this->sWidth > $this->sHeight) {
			$this->tWidth = $tSize;
			$this->tHeight = $this->GetProportionalHeight($tSize);
		} else {
			$this->tHeight = $tSize;
			$this->tWidth = $this->GetProportionalWidth($tSize);
        }
    }
    public function Props() {
        return array($this->tWidth, $this->tHeight);
    }
}

if(isset($_POST)) {
    // get start time
    $start_array = explode(" ", microtime());
    $start_time = $start_array[1] + $start_array[0];
    //-------------------------------------------------------------------------
    $sWidth = $_POST["sWidth"];     //source width
    $sHeight = $_POST["sHeight"];   //source height
    $tWidth = 0;                    //target width
    $tHeight = 0;                   //target height
    $quality = $_POST["quality"];   //tdrget image quality
    $tSize = $_POST["tSize"];       // target size in pixels : 1000 | 160 x 120
    $fileName = $_POST['fileName']; // file name
    $fileFormat = $_POST["format"]; // file type : jpg | png | gif
    $imageData = $_POST['imageData'];// image data (Base64)
    //-------------------------------------------------------------------------
    $upload_dir = $_SERVER["DOCUMENT_ROOT"].'/tmp/';
    $file = $upload_dir . $fileName . '.' . $fileFormat;
    // For transparent background
    if($fileFormat=='png') $isPNG = true; else $isPNG = false;
    //-------------------------------------------------------------------------
    // Get target width & target height
    if($tSize=='') $size = array($sWidth, $sHeight);   // if not set target size
    else if(strpos($tSize, 'x')) $size = explode('x', $tSize);  // if target size format is [160x120]
    else $size = IGetProps::GetProps($sWidth, $sHeight, $tSize); // if target size format is [1000]
    $tWidth = $size[0]; $tHeight = $size[1];
    //-------------------------------------------------------------------------
    // Get ready image
    $imageData = str_replace('data:image/png;base64,', '', $imageData);
    $imageData = str_replace(' ', '+', $imageData);
    $image_resource_id = imagecreatefromstring(base64_decode($imageData));
    $target_layer = GetReadyLayer($image_resource_id, $tWidth, $tHeight, $sWidth, $sHeight, $isPNG);
    //-------------------------------------------------------------------------
    // Save image in chosen format
    switch ($fileFormat) {
        case 'jpg': imagejpeg($target_layer, $file, ($quality+1)*10); break;
        case 'png': imagepng($target_layer, $file, 9-$quality); break;
        case 'gif': imagegif($target_layer, $file); break;
        default : ReturnError('wrong file type'); return; break;
    }
    //-------------------------------------------------------------------------
    // get end time
    $end_array = explode(" ", microtime());
    $end_time = $end_array[1] + $end_array[0];
    // get elapsed time
    $elapsed_time = intval(($end_time - $start_time)*1000);
    //-------------------------------------------------------------------------
    // return image file url link to user 
    $link = '/Tools/Paste Image/tmp/'.$fileName.'.'.$fileFormat;
    echo json_encode(array( 'status' => true, 
                            'link' => $link,
                            'width' => $tWidth,
                            'height' => $tHeight,
                            'time' => $elapsed_time));
} else {
    ReturnError('empty post data!');
}

function ReturnError($reason) // print error reason in JSON 
{
    echo json_encode(array('status' => false, 'reason' => $reason));
}

function GetReadyLayer($image_resource_id, $tWidth, $tHeight, $sWidth, $sHeight, $isPNG) {
    $target_layer = imagecreatetruecolor($tWidth, $tHeight);
    if($isPNG) {
        imagealphablending($target_layer, false);
        imagesavealpha($target_layer, true);
        $transparent = imagecolorallocatealpha($target_layer, 255, 255, 255, 127);
        imagefilledrectangle($target_layer, 0, 0, $sWidth, $sHeight, $transparent);
    }
    imagecopyresampled($target_layer, $image_resource_id,0,0,0,0,
        $tWidth, $tHeight, $sWidth, $sHeight);
    return $target_layer;
}

?>